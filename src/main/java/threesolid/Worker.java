// Rain Lawson

package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;
import threesolid.*;

/*
    Implements the Open Close principle by being available to extension and closed to modification.
    It is extended by the other classes, and is not modified.
*/

class Worker implements IWorker {
	public String name = "";  	
	
		public String getName() 
		{
		  return name;
		}
  
		public void setName(String name) 
		{
			this.name = name;
		}
  
		public String work() 
		{  
  
			if (name == "") 
		  {
				 return "I'm working already!";
		  }
		  else 
		  {
				 return name + " is working very hard!";
		  }
		}

		public Boolean addpositive(int a, int b)
		{
		  if ((a+b) > 0)
			return(true);
		  return(false);
		}
	  

}