// Rain Lawson

package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;
import threesolid.*;

/*
	This implements both Single Responsibility by only handling the stuff for human workers,
	rather than handling the work for 
*/

class HumanWorker extends Worker implements IHuman{

	public String eat() 
	{
		if (name == "") 
    	{
       		return "I'm eating already!";
    	}
    	else 
    	{
    		return name + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";
    	}
	}
}
