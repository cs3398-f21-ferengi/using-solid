// Fernando Perez
package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;
import threesolid.*;

// Single responsability, it adhears to the idea of separating the files to allow each file to conduct one purpose

public class Robot implements IWorker{
	private String name = "";

  	public String getName() 
  	{
    	return name;
  	}

  	public void setName(String name) 
  	{
      	this.name = name;
  	}

  	public String work() 
  	{  

  		if (name == "") 
    	{
       		return "I'm working already!";
    	}
    	else 
    	{
       		return name + " is working very hard!";
    	}
	}

      public Boolean addpositive(int a, int b)
    {
      if ((a+b) > 0)
        return(true);
      return(false);
    }
}