package threesolid;
//This was seperated to abide by the Interface Segregation Principle
interface IWorker{
	public String work();
}