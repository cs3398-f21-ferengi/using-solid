package threesolid;

/*
Jose Tabares
This was separated to abide by the Interface Segregation Principle
*/


interface IHuman {
	public String eat();
}