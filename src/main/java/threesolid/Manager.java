package threesolid;

/*

Jose Tabares
Separated to abide to Single Responsibility Principle

*/


public class Manager {
	IWorker worker;

	public void Manager() {

	}
	public void setWorker(IWorker w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}