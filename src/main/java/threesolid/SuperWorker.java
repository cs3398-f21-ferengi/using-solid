package threesolid;

/*

Jose Tabares
Separated for project structure

*/

class SuperWorker implements IWorker, IHuman{
	public String work() {
		return "I'm a super worker!";
	}

	public String eat() {
		return "I'm eating a super healthy meal";
	}
}
